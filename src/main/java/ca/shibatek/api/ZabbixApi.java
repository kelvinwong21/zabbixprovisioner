package ca.shibatek.api;


enum ZabbixApi {

    USER_LOGIN("user.login"),
    HOSTGROUP_GET("hostgroup.get");

    private String method;

    ZabbixApi(String method) {
        this.method = method;
    }

    public String getMethod() {
        return this.method;
    }


}
