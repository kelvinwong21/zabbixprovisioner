package ca.shibatek.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import lombok.extern.log4j.Log4j2;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;


import java.io.IOException;
import java.util.Properties;

@Log4j2
class ZabbixModule extends AbstractModule {

    @Override
    protected void configure(){

        Properties defaults = new Properties();
        defaults.setProperty("zabbix.json.rpc", "default");
        log.info("Loading properties file");
        try{
            Properties properties = new Properties(defaults);
            properties.load(this.getClass().getClassLoader().getResourceAsStream("zabbix-provisioner.properties"));
            Names.bindProperties(binder(), properties);
        } catch (IOException e){
            log.error("Could not load properties file", e);
            System.exit(1);
        }
    }

    @Provides
    ApiCaller provideApiCaller(@Named("zabbix.json.rpc") final String rpcUrl,
                               final Gson gson, final HttpClient httpClient, final JsonParser jsonParser) {

        log.info("Providing ApiCaller");
        return new ApiCaller(httpClient, gson, rpcUrl, jsonParser);
    }

    @Provides
    Gson provideGson() {

        return new GsonBuilder().create();
    }

    @Provides
    HttpClient provideHttpClient() {

        return HttpClientBuilder.create().build();
    }

    @Provides
    JsonParser jsonParser() {

        return new JsonParser();
    }

}
