package ca.shibatek.api.model.input;

import lombok.Data;

@Data
public class HostInterfaceDTO {

    private String type;
    private String ip;
    private String dns;
    private String port;
    private boolean main;
    private boolean useIp;
}
