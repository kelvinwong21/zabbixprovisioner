package ca.shibatek.api.model.zabbix;

import java.util.ArrayList;
import java.util.List;

public class GetModel {

    private boolean countOutput;
    private boolean searchWildcardsEnabled;
    private Filter filter;
    private String output;
    private String sortOrder;

    private GetModel(){
    }

    public static class Builder {

        private boolean countOutput;
        private boolean searchWildcardsEnabled;
        private String output;
        private String sortOrder;
        private Filter filter;

        public Builder doesCountOutput(boolean countOutput){
            this.countOutput = countOutput;

            return this;
        }

        public Builder doesSearchWildcardsEnabled(boolean searchWildcardsEnabled){
            this.searchWildcardsEnabled = searchWildcardsEnabled;

            return this;
        }

        public Builder output(String output){
            this.output = output;

            return this;
        }

        public Builder sortOrder(String sortOrder){
            this.sortOrder = sortOrder;

            return this;
        }

        public Builder withFilter(){
            this.filter = new Filter();

            return this;
        }

        public Builder addFilter(String name){
            filter.addFilter(name);

            return this;
        }

        public GetModel build(){
            GetModel getModel = new GetModel();

            getModel.countOutput = this.countOutput;
            getModel.searchWildcardsEnabled = this.searchWildcardsEnabled;
            getModel.filter = this.filter;
            getModel.output = this.output;
            getModel.sortOrder = this.sortOrder;

            return getModel;
        }
    }

    public static class Filter{

        private final List<String> name;

        private Filter(){
            this.name = new ArrayList<>();
        }

        public void addFilter(String filter){
            name.add(filter);
        }
    }

}
