package ca.shibatek.api.util;

import com.google.gson.JsonObject;

public class ResponseValidator {

    public static final String ERROR = "error";
    public static final String RESULT = "result";

    public static boolean hasError(JsonObject jsonResponse){
        return jsonResponse.has(ERROR);
    }

    public static String getAuthToken(JsonObject jsonResponse){
        return jsonResponse.get(RESULT).getAsString();
    }
}
