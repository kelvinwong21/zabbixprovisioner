package ca.shibatek.api;

import ca.shibatek.api.model.zabbix.ZabbixParams;
import ca.shibatek.api.model.zabbix.ZabbixRequestModel;
import ca.shibatek.api.model.zabbix.UserLogin;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApiCallerTest {

    private static final String JSON_RPC =  "https://annakendrick.com";
    private static final InputStream VALID_RESPONSE = new ByteArrayInputStream("{ \"hello\" : \"world\" }".getBytes());
    private static final InputStream ERROR_RESPONSE = new ByteArrayInputStream("{ \"error\" : \"message\" }".getBytes());
    @Mock
    private HttpClient httpClient;
    @Mock
    private HttpResponse httpResponse;
    @Mock
    private HttpEntity httpEntity;
    private ZabbixRequestModel request;
    private Gson gson;
    private JsonParser parser;
    private ApiCaller apiCaller;

    @Before
    public void setUp() {
        ZabbixParams params = new UserLogin("anna", "kendrick");
        this.request = new ZabbixRequestModel(params);
        this.gson =  new GsonBuilder().create();
        this.parser = new JsonParser();
        this.apiCaller = new ApiCaller(httpClient, gson, JSON_RPC, parser);
    }

    @Test
    public void shouldReturnAResponseWithValidApiCall() throws Exception {

        when(httpClient.execute(any(HttpPost.class))).thenReturn(httpResponse);
        when(httpResponse.getEntity()).thenReturn(httpEntity);
        when(httpEntity.getContent()).thenReturn(VALID_RESPONSE);

        final JsonObject response = apiCaller.makeCall(request);

        assertThat(response.size(), not(equalTo(0)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void sholdThrowExceptionIfErrorIsInResponse() throws Exception {

        when(httpClient.execute(any(HttpPost.class))).thenReturn(httpResponse);
        when(httpResponse.getEntity()).thenReturn(httpEntity);
        when(httpEntity.getContent()).thenReturn(ERROR_RESPONSE);

        final JsonObject response = apiCaller.makeCall(request);
    }
}
