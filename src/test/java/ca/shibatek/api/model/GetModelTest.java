package ca.shibatek.api.model;

import ca.shibatek.api.model.zabbix.GetModel;
import com.google.gson.Gson;
import org.junit.Test;

public class GetModelTest {

    @Test
    public void shouldBuildGetModelWithSearchDesc(){
        GetModel getModel = new GetModel.Builder()
                .output("extend")
                .doesCountOutput(true)
                .withFilter()
                .addFilter("Zabbix servers")
                .addFilter("Linux servers")
                .build();

        Gson gson = new Gson();

        String json = gson.toJson(getModel);

        System.out.println(json);
    }
}
