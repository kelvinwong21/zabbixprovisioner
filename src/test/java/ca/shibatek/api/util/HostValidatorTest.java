package ca.shibatek.api.util;

import ca.shibatek.api.model.input.HostDTO;
import ca.shibatek.api.model.input.HostInterfaceDTO;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;

public class HostValidatorTest {

    @Test
    public void shouldRemoveHostDTOIfContainsInvalidFields(){
        List<HostDTO> hostDTOList = new ArrayList<>();
        List<String> stringList = new ArrayList<>();
        stringList.add("Anna Kendrick");

        HostDTO validHostDTO = new HostDTO();
        HostInterfaceDTO validHostInterfaceDTO = new HostInterfaceDTO();
        validHostInterfaceDTO.setDns("annakendrick.com");
        validHostInterfaceDTO.setPort("1050");
        validHostDTO.setHost("Test 1");
        validHostDTO.setTemplates(stringList);
        validHostDTO.setGroups(stringList);
        validHostDTO.setInterfaces(validHostInterfaceDTO);

        HostDTO invalidHostDTO = new HostDTO();
        HostInterfaceDTO invalidHostInterfaceDTO = new HostInterfaceDTO();
        invalidHostDTO.setInterfaces(invalidHostInterfaceDTO);

        hostDTOList.add(validHostDTO);
        hostDTOList.add(invalidHostDTO);

        HostValidator.removeInvalidHostDTO(hostDTOList);

        assertThat(hostDTOList, hasSize(1));
    }
}